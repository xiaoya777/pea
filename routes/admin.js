const express=require('express')
const pool=require('../module/pool')
const router=express.Router()
//登录接口 post  
//url:  /admin/login
//约定传参的属性名   anames  apwd
//访问的数据表:admin anames apwd
router.post('/login',(req,res,next)=>{
  //console.log('第9行检查登录接口连接成功')
  let obj=req.body
  //console.log(obj)
  pool.query('select * from admin where anames=? and apwd=?;',[obj.anames,obj.apwd],(err,data)=>{
    console.log(data)
    if(err){
      next(err)
      return
    }
    if(data.length==0){
      res.send({
        "code":0,
        "msg":"用户名或密码错误,登录失败"
      })
    }else{
      res.send({
        "code":1,
        "msg":"登陆成功,欢迎使用"
      })
    }
  })
})

//管理员查询接口
//  /admin/list
router.get('/list',(req,res,next)=>{
  let obj=req.query
  //console.log(obj)
  pool.query('select * from pea_user;',(err,data)=>{
    if(err){next(err);return;}
    res.send({
      "code":1,
      "msg":"返回所有用户数据",
      "data":data
    })
  })
})
module.exports=router