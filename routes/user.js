const express=require('express')
const pool=require('../module/pool.js')
const router=express.Router()
//用户登录接口
//pea_user数据表    phone upwd
router.post('/login',(req,res,next)=>{
  let obj=req.body
  //console.log(obj)
  pool.query('select * from pea_user where upwd=? and phone=?',[obj.upwd,obj.phone],(err,data)=>{
    //console.log(data)
    if(err){next(err);return;}
    if(data.length==1){
      res.send({
        "code":1,
        "msg":"登录成功"
      })
    }else{
      res.send({
          "code":0,
          "msg":"手机号或密码错误,登录失败"
      })
    }
  })
})


module.exports=router