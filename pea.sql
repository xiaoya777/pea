set names utf8;
drop database if exists pea;
create database pea charset = utf8;
use pea;
create table pea_user(
  u_id int primary key not null auto_increment,
  unames varchar(32),
  upwd varchar(32),
  phone varchar(16) not null unique,
  grade varchar(8),
  gender int,
  province varchar(16),
  email varchar(64),
  avatar varchar(128)
) ENGINE = InnoDB charset = utf8; 

insert into pea_user(unames,upwd,phone) values
('tom','wydxb555','15201076723'),
('nancy','wydxb555','15201197440'),
('jack','wydxb555','15101075644'),
('bob','wydxb555','15910257181'),
('lily','wydxb555','13522162834'),
('Kate','wydxb555','13661217464'),
('emma','wydxb555','13522162843'),
('bale','wydxb666','13671357146'),
('coy','wydxb666','13522172419'),
('ball','wydxb666','15910779501'),
('dan','wydxb666','15910780895');

create table admin(
  a_id int auto_increment primary key,
  anames varchar(255) not null unique,
  apwd varchar(255) not null
) ENGINE = InnoDB charset = utf8;

INSERT INTO
  admin(anames, apwd)
VALUES
  ('zhangsan', '123'),
  ('lisi', '123'),
  ('wangwu', '123');