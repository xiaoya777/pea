const express=require('express');
const app=express();
app.listen(8080,()=>{
  console.log('♥ ♥ ♥')
})
//静态资源
app.use(express.static('./public'))
app.use(express.static('./views'))
//post传参解析
app.use(express.urlencoded({
  extended:false
}))
//引入路由
const userRouter=require('./routes/user')
const adminRouter=require('./routes/admin')
app.use('/admin',adminRouter)
app.use('/user',userRouter)
//错误处理中间件
app.use((err,req,res,next)=>{
  console.log(err)
  res.status(500).send({
    "code":500,
    "msg":"服务器端错误"
  })
})